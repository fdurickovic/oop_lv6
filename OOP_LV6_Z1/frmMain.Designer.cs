﻿namespace OOP_LV6_Z1 {
	partial class frmMain {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
			this.btn1 = new System.Windows.Forms.Button();
			this.btn2 = new System.Windows.Forms.Button();
			this.btn3 = new System.Windows.Forms.Button();
			this.btn4 = new System.Windows.Forms.Button();
			this.btn5 = new System.Windows.Forms.Button();
			this.btn6 = new System.Windows.Forms.Button();
			this.btn7 = new System.Windows.Forms.Button();
			this.btn8 = new System.Windows.Forms.Button();
			this.btn9 = new System.Windows.Forms.Button();
			this.btn0 = new System.Windows.Forms.Button();
			this.btnBackspace = new System.Windows.Forms.Button();
			this.btnC = new System.Windows.Forms.Button();
			this.btnCE = new System.Windows.Forms.Button();
			this.btnSubtract = new System.Windows.Forms.Button();
			this.btnMultiply = new System.Windows.Forms.Button();
			this.btnDivide = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnTanInv = new System.Windows.Forms.Button();
			this.btnCosInv = new System.Windows.Forms.Button();
			this.btnSinInv = new System.Windows.Forms.Button();
			this.btnLn = new System.Windows.Forms.Button();
			this.btnLog = new System.Windows.Forms.Button();
			this.btnTan = new System.Windows.Forms.Button();
			this.btnCos = new System.Windows.Forms.Button();
			this.btnSin = new System.Windows.Forms.Button();
			this.btnMod = new System.Windows.Forms.Button();
			this.btnFactorial = new System.Windows.Forms.Button();
			this.btnPow = new System.Windows.Forms.Button();
			this.btnNthRoot = new System.Windows.Forms.Button();
			this.btnE = new System.Windows.Forms.Button();
			this.btnExp = new System.Windows.Forms.Button();
			this.btnInv = new System.Windows.Forms.Button();
			this.btnSquareRoot = new System.Windows.Forms.Button();
			this.btnCube = new System.Windows.Forms.Button();
			this.btnSquare = new System.Windows.Forms.Button();
			this.btnSwitchSign = new System.Windows.Forms.Button();
			this.btnDecSeparator = new System.Windows.Forms.Button();
			this.btnCalculate = new System.Windows.Forms.Button();
			this.tbResult = new System.Windows.Forms.TextBox();
			this.btnPercent = new System.Windows.Forms.Button();
			this.btnCubeRoot = new System.Windows.Forms.Button();
			this.btnPi = new System.Windows.Forms.Button();
			this.btnSinHiperbolic = new System.Windows.Forms.Button();
			this.btnCosHiperbolic = new System.Windows.Forms.Button();
			this.btnTanHiperbolic = new System.Windows.Forms.Button();
			this.btnSinHiperbolicInv = new System.Windows.Forms.Button();
			this.btnCosHiperbolicInv = new System.Windows.Forms.Button();
			this.btnTanHiperbolicInv = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btn1
			// 
			this.btn1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn1.Location = new System.Drawing.Point(273, 155);
			this.btn1.Name = "btn1";
			this.btn1.Size = new System.Drawing.Size(30, 30);
			this.btn1.TabIndex = 1;
			this.btn1.TabStop = false;
			this.btn1.Text = "1";
			this.btn1.UseCompatibleTextRendering = true;
			this.btn1.UseVisualStyleBackColor = false;
			this.btn1.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btn2
			// 
			this.btn2.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn2.Location = new System.Drawing.Point(309, 155);
			this.btn2.Name = "btn2";
			this.btn2.Size = new System.Drawing.Size(30, 30);
			this.btn2.TabIndex = 2;
			this.btn2.TabStop = false;
			this.btn2.Text = "2";
			this.btn2.UseCompatibleTextRendering = true;
			this.btn2.UseVisualStyleBackColor = false;
			this.btn2.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btn3
			// 
			this.btn3.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn3.Location = new System.Drawing.Point(345, 155);
			this.btn3.Name = "btn3";
			this.btn3.Size = new System.Drawing.Size(30, 30);
			this.btn3.TabIndex = 3;
			this.btn3.TabStop = false;
			this.btn3.Text = "3";
			this.btn3.UseCompatibleTextRendering = true;
			this.btn3.UseVisualStyleBackColor = false;
			this.btn3.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btn4
			// 
			this.btn4.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn4.Location = new System.Drawing.Point(273, 119);
			this.btn4.Name = "btn4";
			this.btn4.Size = new System.Drawing.Size(30, 30);
			this.btn4.TabIndex = 4;
			this.btn4.TabStop = false;
			this.btn4.Text = "4";
			this.btn4.UseCompatibleTextRendering = true;
			this.btn4.UseVisualStyleBackColor = false;
			this.btn4.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btn5
			// 
			this.btn5.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn5.Location = new System.Drawing.Point(309, 119);
			this.btn5.Name = "btn5";
			this.btn5.Size = new System.Drawing.Size(30, 30);
			this.btn5.TabIndex = 5;
			this.btn5.TabStop = false;
			this.btn5.Text = "5";
			this.btn5.UseCompatibleTextRendering = true;
			this.btn5.UseVisualStyleBackColor = false;
			this.btn5.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btn6
			// 
			this.btn6.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn6.Location = new System.Drawing.Point(345, 119);
			this.btn6.Name = "btn6";
			this.btn6.Size = new System.Drawing.Size(30, 30);
			this.btn6.TabIndex = 6;
			this.btn6.TabStop = false;
			this.btn6.Text = "6";
			this.btn6.UseCompatibleTextRendering = true;
			this.btn6.UseVisualStyleBackColor = false;
			this.btn6.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btn7
			// 
			this.btn7.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn7.Location = new System.Drawing.Point(273, 83);
			this.btn7.Name = "btn7";
			this.btn7.Size = new System.Drawing.Size(30, 30);
			this.btn7.TabIndex = 7;
			this.btn7.TabStop = false;
			this.btn7.Text = "7";
			this.btn7.UseCompatibleTextRendering = true;
			this.btn7.UseVisualStyleBackColor = false;
			this.btn7.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btn8
			// 
			this.btn8.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn8.Location = new System.Drawing.Point(309, 83);
			this.btn8.Name = "btn8";
			this.btn8.Size = new System.Drawing.Size(30, 30);
			this.btn8.TabIndex = 8;
			this.btn8.TabStop = false;
			this.btn8.Text = "8";
			this.btn8.UseCompatibleTextRendering = true;
			this.btn8.UseVisualStyleBackColor = false;
			this.btn8.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btn9
			// 
			this.btn9.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn9.Location = new System.Drawing.Point(345, 83);
			this.btn9.Name = "btn9";
			this.btn9.Size = new System.Drawing.Size(30, 30);
			this.btn9.TabIndex = 9;
			this.btn9.TabStop = false;
			this.btn9.Text = "9";
			this.btn9.UseCompatibleTextRendering = true;
			this.btn9.UseVisualStyleBackColor = false;
			this.btn9.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btn0
			// 
			this.btn0.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btn0.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btn0.Location = new System.Drawing.Point(273, 191);
			this.btn0.Name = "btn0";
			this.btn0.Size = new System.Drawing.Size(66, 30);
			this.btn0.TabIndex = 0;
			this.btn0.TabStop = false;
			this.btn0.Text = "0";
			this.btn0.UseCompatibleTextRendering = true;
			this.btn0.UseVisualStyleBackColor = false;
			this.btn0.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btn0.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnBackspace
			// 
			this.btnBackspace.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnBackspace.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnBackspace.Location = new System.Drawing.Point(273, 47);
			this.btnBackspace.Name = "btnBackspace";
			this.btnBackspace.Size = new System.Drawing.Size(30, 30);
			this.btnBackspace.TabIndex = 14;
			this.btnBackspace.TabStop = false;
			this.btnBackspace.Text = "⌫";
			this.btnBackspace.UseCompatibleTextRendering = true;
			this.btnBackspace.UseVisualStyleBackColor = false;
			this.btnBackspace.Click += new System.EventHandler(this.btnBackspace_Click);
			this.btnBackspace.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnC
			// 
			this.btnC.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnC.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnC.Location = new System.Drawing.Point(345, 47);
			this.btnC.Name = "btnC";
			this.btnC.Size = new System.Drawing.Size(30, 30);
			this.btnC.TabIndex = 13;
			this.btnC.TabStop = false;
			this.btnC.Text = "C";
			this.btnC.UseCompatibleTextRendering = true;
			this.btnC.UseVisualStyleBackColor = false;
			this.btnC.Click += new System.EventHandler(this.btnC_Click);
			this.btnC.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnCE
			// 
			this.btnCE.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCE.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCE.Location = new System.Drawing.Point(309, 47);
			this.btnCE.Name = "btnCE";
			this.btnCE.Size = new System.Drawing.Size(30, 30);
			this.btnCE.TabIndex = 12;
			this.btnCE.TabStop = false;
			this.btnCE.Text = "CE";
			this.btnCE.UseCompatibleTextRendering = true;
			this.btnCE.UseVisualStyleBackColor = false;
			this.btnCE.Click += new System.EventHandler(this.btnCE_Click);
			this.btnCE.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnSubtract
			// 
			this.btnSubtract.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSubtract.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnSubtract.Location = new System.Drawing.Point(381, 155);
			this.btnSubtract.Name = "btnSubtract";
			this.btnSubtract.Size = new System.Drawing.Size(30, 30);
			this.btnSubtract.TabIndex = 17;
			this.btnSubtract.TabStop = false;
			this.btnSubtract.Text = "-";
			this.btnSubtract.UseCompatibleTextRendering = true;
			this.btnSubtract.UseVisualStyleBackColor = false;
			this.btnSubtract.Click += new System.EventHandler(this.btnOperator_Click);
			this.btnSubtract.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnMultiply
			// 
			this.btnMultiply.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnMultiply.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnMultiply.Location = new System.Drawing.Point(381, 119);
			this.btnMultiply.Name = "btnMultiply";
			this.btnMultiply.Size = new System.Drawing.Size(30, 30);
			this.btnMultiply.TabIndex = 16;
			this.btnMultiply.TabStop = false;
			this.btnMultiply.Text = "×";
			this.btnMultiply.UseCompatibleTextRendering = true;
			this.btnMultiply.UseVisualStyleBackColor = false;
			this.btnMultiply.Click += new System.EventHandler(this.btnOperator_Click);
			this.btnMultiply.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnDivide
			// 
			this.btnDivide.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnDivide.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnDivide.Location = new System.Drawing.Point(381, 83);
			this.btnDivide.Name = "btnDivide";
			this.btnDivide.Size = new System.Drawing.Size(30, 30);
			this.btnDivide.TabIndex = 15;
			this.btnDivide.TabStop = false;
			this.btnDivide.Text = "÷";
			this.btnDivide.UseCompatibleTextRendering = true;
			this.btnDivide.UseVisualStyleBackColor = false;
			this.btnDivide.Click += new System.EventHandler(this.btnOperator_Click);
			this.btnDivide.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnAdd
			// 
			this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnAdd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnAdd.Location = new System.Drawing.Point(381, 191);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(30, 30);
			this.btnAdd.TabIndex = 18;
			this.btnAdd.TabStop = false;
			this.btnAdd.Text = "+";
			this.btnAdd.UseCompatibleTextRendering = true;
			this.btnAdd.UseVisualStyleBackColor = false;
			this.btnAdd.Click += new System.EventHandler(this.btnOperator_Click);
			this.btnAdd.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnTanInv
			// 
			this.btnTanInv.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnTanInv.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnTanInv.Location = new System.Drawing.Point(114, 83);
			this.btnTanInv.Name = "btnTanInv";
			this.btnTanInv.Size = new System.Drawing.Size(45, 30);
			this.btnTanInv.TabIndex = 35;
			this.btnTanInv.TabStop = false;
			this.btnTanInv.Text = "tan⁻¹";
			this.btnTanInv.UseCompatibleTextRendering = true;
			this.btnTanInv.UseVisualStyleBackColor = false;
			this.btnTanInv.Click += new System.EventHandler(this.btnTanInv_Click);
			this.btnTanInv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnCosInv
			// 
			this.btnCosInv.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCosInv.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCosInv.Location = new System.Drawing.Point(63, 83);
			this.btnCosInv.Name = "btnCosInv";
			this.btnCosInv.Size = new System.Drawing.Size(45, 30);
			this.btnCosInv.TabIndex = 36;
			this.btnCosInv.TabStop = false;
			this.btnCosInv.Text = "cos⁻¹";
			this.btnCosInv.UseCompatibleTextRendering = true;
			this.btnCosInv.UseVisualStyleBackColor = false;
			this.btnCosInv.Click += new System.EventHandler(this.btnCosInv_Click);
			this.btnCosInv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnSinInv
			// 
			this.btnSinInv.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSinInv.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnSinInv.Location = new System.Drawing.Point(12, 83);
			this.btnSinInv.Name = "btnSinInv";
			this.btnSinInv.Size = new System.Drawing.Size(45, 30);
			this.btnSinInv.TabIndex = 37;
			this.btnSinInv.TabStop = false;
			this.btnSinInv.Text = "sin⁻¹";
			this.btnSinInv.UseCompatibleTextRendering = true;
			this.btnSinInv.UseVisualStyleBackColor = false;
			this.btnSinInv.Click += new System.EventHandler(this.btnSinInv_Click);
			this.btnSinInv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnLn
			// 
			this.btnLn.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnLn.Location = new System.Drawing.Point(165, 119);
			this.btnLn.Name = "btnLn";
			this.btnLn.Size = new System.Drawing.Size(30, 30);
			this.btnLn.TabIndex = 30;
			this.btnLn.TabStop = false;
			this.btnLn.Text = "ln";
			this.btnLn.UseCompatibleTextRendering = true;
			this.btnLn.UseVisualStyleBackColor = false;
			this.btnLn.Click += new System.EventHandler(this.btnLn_Click);
			this.btnLn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnLog
			// 
			this.btnLog.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLog.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnLog.Location = new System.Drawing.Point(165, 83);
			this.btnLog.Name = "btnLog";
			this.btnLog.Size = new System.Drawing.Size(30, 30);
			this.btnLog.TabIndex = 31;
			this.btnLog.TabStop = false;
			this.btnLog.Text = "log";
			this.btnLog.UseCompatibleTextRendering = true;
			this.btnLog.UseVisualStyleBackColor = false;
			this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
			this.btnLog.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnTan
			// 
			this.btnTan.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnTan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnTan.Location = new System.Drawing.Point(114, 47);
			this.btnTan.Name = "btnTan";
			this.btnTan.Size = new System.Drawing.Size(45, 30);
			this.btnTan.TabIndex = 32;
			this.btnTan.TabStop = false;
			this.btnTan.Text = "tan";
			this.btnTan.UseCompatibleTextRendering = true;
			this.btnTan.UseVisualStyleBackColor = false;
			this.btnTan.Click += new System.EventHandler(this.btnTan_Click);
			this.btnTan.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnCos
			// 
			this.btnCos.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCos.Location = new System.Drawing.Point(63, 47);
			this.btnCos.Name = "btnCos";
			this.btnCos.Size = new System.Drawing.Size(45, 30);
			this.btnCos.TabIndex = 33;
			this.btnCos.TabStop = false;
			this.btnCos.Text = "cos";
			this.btnCos.UseCompatibleTextRendering = true;
			this.btnCos.UseVisualStyleBackColor = false;
			this.btnCos.Click += new System.EventHandler(this.btnCos_Click);
			this.btnCos.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnSin
			// 
			this.btnSin.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSin.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnSin.Location = new System.Drawing.Point(12, 47);
			this.btnSin.Name = "btnSin";
			this.btnSin.Size = new System.Drawing.Size(45, 30);
			this.btnSin.TabIndex = 34;
			this.btnSin.TabStop = false;
			this.btnSin.Text = "sin";
			this.btnSin.UseCompatibleTextRendering = true;
			this.btnSin.UseVisualStyleBackColor = false;
			this.btnSin.Click += new System.EventHandler(this.btnSin_Click);
			this.btnSin.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnMod
			// 
			this.btnMod.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnMod.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnMod.Location = new System.Drawing.Point(165, 47);
			this.btnMod.Name = "btnMod";
			this.btnMod.Size = new System.Drawing.Size(66, 30);
			this.btnMod.TabIndex = 25;
			this.btnMod.TabStop = false;
			this.btnMod.Text = "Mod";
			this.btnMod.UseCompatibleTextRendering = true;
			this.btnMod.UseVisualStyleBackColor = false;
			this.btnMod.Click += new System.EventHandler(this.btnOperator_Click);
			// 
			// btnFactorial
			// 
			this.btnFactorial.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnFactorial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnFactorial.Location = new System.Drawing.Point(201, 119);
			this.btnFactorial.Name = "btnFactorial";
			this.btnFactorial.Size = new System.Drawing.Size(30, 30);
			this.btnFactorial.TabIndex = 26;
			this.btnFactorial.TabStop = false;
			this.btnFactorial.Text = "n!";
			this.btnFactorial.UseCompatibleTextRendering = true;
			this.btnFactorial.UseVisualStyleBackColor = false;
			this.btnFactorial.Click += new System.EventHandler(this.btnFactorial_Click);
			// 
			// btnPow
			// 
			this.btnPow.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPow.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnPow.Location = new System.Drawing.Point(237, 119);
			this.btnPow.Name = "btnPow";
			this.btnPow.Size = new System.Drawing.Size(30, 30);
			this.btnPow.TabIndex = 22;
			this.btnPow.TabStop = false;
			this.btnPow.Text = "xⁿ";
			this.btnPow.UseCompatibleTextRendering = true;
			this.btnPow.UseVisualStyleBackColor = false;
			this.btnPow.Click += new System.EventHandler(this.btnOperator_Click);
			this.btnPow.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnNthRoot
			// 
			this.btnNthRoot.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnNthRoot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnNthRoot.Location = new System.Drawing.Point(165, 155);
			this.btnNthRoot.Name = "btnNthRoot";
			this.btnNthRoot.Size = new System.Drawing.Size(30, 30);
			this.btnNthRoot.TabIndex = 28;
			this.btnNthRoot.TabStop = false;
			this.btnNthRoot.Text = "ⁿ√x";
			this.btnNthRoot.UseCompatibleTextRendering = true;
			this.btnNthRoot.UseVisualStyleBackColor = false;
			this.btnNthRoot.Click += new System.EventHandler(this.btnOperator_Click);
			this.btnNthRoot.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnE
			// 
			this.btnE.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnE.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnE.Location = new System.Drawing.Point(237, 191);
			this.btnE.Name = "btnE";
			this.btnE.Size = new System.Drawing.Size(30, 30);
			this.btnE.TabIndex = 29;
			this.btnE.TabStop = false;
			this.btnE.Text = "10ⁿ";
			this.btnE.UseCompatibleTextRendering = true;
			this.btnE.UseVisualStyleBackColor = false;
			this.btnE.Click += new System.EventHandler(this.btnE_Click);
			this.btnE.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnExp
			// 
			this.btnExp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnExp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnExp.Location = new System.Drawing.Point(237, 155);
			this.btnExp.Name = "btnExp";
			this.btnExp.Size = new System.Drawing.Size(30, 30);
			this.btnExp.TabIndex = 20;
			this.btnExp.TabStop = false;
			this.btnExp.Text = "eⁿ";
			this.btnExp.UseCompatibleTextRendering = true;
			this.btnExp.UseVisualStyleBackColor = false;
			this.btnExp.Click += new System.EventHandler(this.btnExp_Click);
			this.btnExp.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnInv
			// 
			this.btnInv.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnInv.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnInv.Location = new System.Drawing.Point(417, 119);
			this.btnInv.Name = "btnInv";
			this.btnInv.Size = new System.Drawing.Size(30, 30);
			this.btnInv.TabIndex = 21;
			this.btnInv.TabStop = false;
			this.btnInv.Text = "¹⁄ₓ";
			this.btnInv.UseCompatibleTextRendering = true;
			this.btnInv.UseVisualStyleBackColor = false;
			this.btnInv.Click += new System.EventHandler(this.btnInv_Click);
			this.btnInv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnSquareRoot
			// 
			this.btnSquareRoot.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSquareRoot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnSquareRoot.Location = new System.Drawing.Point(417, 47);
			this.btnSquareRoot.Name = "btnSquareRoot";
			this.btnSquareRoot.Size = new System.Drawing.Size(30, 30);
			this.btnSquareRoot.TabIndex = 27;
			this.btnSquareRoot.TabStop = false;
			this.btnSquareRoot.Text = "√";
			this.btnSquareRoot.UseCompatibleTextRendering = true;
			this.btnSquareRoot.UseVisualStyleBackColor = false;
			this.btnSquareRoot.Click += new System.EventHandler(this.btnSquareRoot_Click);
			this.btnSquareRoot.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnCube
			// 
			this.btnCube.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCube.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCube.Location = new System.Drawing.Point(237, 83);
			this.btnCube.Name = "btnCube";
			this.btnCube.Size = new System.Drawing.Size(30, 30);
			this.btnCube.TabIndex = 23;
			this.btnCube.TabStop = false;
			this.btnCube.Text = "x³";
			this.btnCube.UseCompatibleTextRendering = true;
			this.btnCube.UseVisualStyleBackColor = false;
			this.btnCube.Click += new System.EventHandler(this.btnCube_Click);
			this.btnCube.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnSquare
			// 
			this.btnSquare.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSquare.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnSquare.Location = new System.Drawing.Point(237, 47);
			this.btnSquare.Name = "btnSquare";
			this.btnSquare.Size = new System.Drawing.Size(30, 30);
			this.btnSquare.TabIndex = 24;
			this.btnSquare.TabStop = false;
			this.btnSquare.Text = "x²";
			this.btnSquare.UseCompatibleTextRendering = true;
			this.btnSquare.UseVisualStyleBackColor = false;
			this.btnSquare.Click += new System.EventHandler(this.btnSquare_Click);
			this.btnSquare.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnSwitchSign
			// 
			this.btnSwitchSign.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSwitchSign.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnSwitchSign.Location = new System.Drawing.Point(381, 47);
			this.btnSwitchSign.Name = "btnSwitchSign";
			this.btnSwitchSign.Size = new System.Drawing.Size(30, 30);
			this.btnSwitchSign.TabIndex = 11;
			this.btnSwitchSign.TabStop = false;
			this.btnSwitchSign.Text = "±";
			this.btnSwitchSign.UseCompatibleTextRendering = true;
			this.btnSwitchSign.UseVisualStyleBackColor = false;
			this.btnSwitchSign.Click += new System.EventHandler(this.btnSwitchSign_Click);
			this.btnSwitchSign.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnDecSeparator
			// 
			this.btnDecSeparator.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnDecSeparator.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnDecSeparator.Location = new System.Drawing.Point(345, 191);
			this.btnDecSeparator.Name = "btnDecSeparator";
			this.btnDecSeparator.Size = new System.Drawing.Size(30, 30);
			this.btnDecSeparator.TabIndex = 10;
			this.btnDecSeparator.TabStop = false;
			this.btnDecSeparator.Text = "DEC";
			this.btnDecSeparator.UseCompatibleTextRendering = true;
			this.btnDecSeparator.UseVisualStyleBackColor = false;
			this.btnDecSeparator.Click += new System.EventHandler(this.btnNumeric_Click);
			this.btnDecSeparator.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnCalculate
			// 
			this.btnCalculate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCalculate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCalculate.Location = new System.Drawing.Point(417, 155);
			this.btnCalculate.Name = "btnCalculate";
			this.btnCalculate.Size = new System.Drawing.Size(30, 66);
			this.btnCalculate.TabIndex = 19;
			this.btnCalculate.TabStop = false;
			this.btnCalculate.Text = "=";
			this.btnCalculate.UseCompatibleTextRendering = true;
			this.btnCalculate.UseVisualStyleBackColor = false;
			this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
			this.btnCalculate.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// tbResult
			// 
			this.tbResult.BackColor = System.Drawing.SystemColors.Window;
			this.tbResult.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
			this.tbResult.Location = new System.Drawing.Point(12, 12);
			this.tbResult.Name = "tbResult";
			this.tbResult.ReadOnly = true;
			this.tbResult.Size = new System.Drawing.Size(435, 29);
			this.tbResult.TabIndex = 45;
			this.tbResult.Text = "0";
			this.tbResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tbResult.TextChanged += new System.EventHandler(this.tbResult_TextChanged);
			// 
			// btnPercent
			// 
			this.btnPercent.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPercent.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnPercent.Location = new System.Drawing.Point(417, 83);
			this.btnPercent.Name = "btnPercent";
			this.btnPercent.Size = new System.Drawing.Size(30, 30);
			this.btnPercent.TabIndex = 46;
			this.btnPercent.TabStop = false;
			this.btnPercent.Text = "%";
			this.btnPercent.UseCompatibleTextRendering = true;
			this.btnPercent.UseVisualStyleBackColor = false;
			this.btnPercent.Click += new System.EventHandler(this.btnPercent_Click);
			this.btnPercent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnCubeRoot
			// 
			this.btnCubeRoot.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCubeRoot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCubeRoot.Location = new System.Drawing.Point(201, 155);
			this.btnCubeRoot.Name = "btnCubeRoot";
			this.btnCubeRoot.Size = new System.Drawing.Size(30, 30);
			this.btnCubeRoot.TabIndex = 47;
			this.btnCubeRoot.TabStop = false;
			this.btnCubeRoot.Text = "³√x";
			this.btnCubeRoot.UseCompatibleTextRendering = true;
			this.btnCubeRoot.UseVisualStyleBackColor = false;
			this.btnCubeRoot.Click += new System.EventHandler(this.btnCubeRoot_Click);
			// 
			// btnPi
			// 
			this.btnPi.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnPi.Location = new System.Drawing.Point(201, 83);
			this.btnPi.Name = "btnPi";
			this.btnPi.Size = new System.Drawing.Size(30, 30);
			this.btnPi.TabIndex = 48;
			this.btnPi.TabStop = false;
			this.btnPi.Text = "π";
			this.btnPi.UseCompatibleTextRendering = true;
			this.btnPi.UseVisualStyleBackColor = false;
			this.btnPi.Click += new System.EventHandler(this.btnPi_Click);
			// 
			// btnSinHiperbolic
			// 
			this.btnSinHiperbolic.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSinHiperbolic.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnSinHiperbolic.Location = new System.Drawing.Point(12, 119);
			this.btnSinHiperbolic.Name = "btnSinHiperbolic";
			this.btnSinHiperbolic.Size = new System.Drawing.Size(45, 30);
			this.btnSinHiperbolic.TabIndex = 51;
			this.btnSinHiperbolic.TabStop = false;
			this.btnSinHiperbolic.Text = "sinh";
			this.btnSinHiperbolic.UseCompatibleTextRendering = true;
			this.btnSinHiperbolic.UseVisualStyleBackColor = false;
			this.btnSinHiperbolic.Click += new System.EventHandler(this.btnSinHiperbolic_Click);
			this.btnSinHiperbolic.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnCosHiperbolic
			// 
			this.btnCosHiperbolic.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCosHiperbolic.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCosHiperbolic.Location = new System.Drawing.Point(63, 119);
			this.btnCosHiperbolic.Name = "btnCosHiperbolic";
			this.btnCosHiperbolic.Size = new System.Drawing.Size(45, 30);
			this.btnCosHiperbolic.TabIndex = 50;
			this.btnCosHiperbolic.TabStop = false;
			this.btnCosHiperbolic.Text = "cosh";
			this.btnCosHiperbolic.UseCompatibleTextRendering = true;
			this.btnCosHiperbolic.UseVisualStyleBackColor = false;
			this.btnCosHiperbolic.Click += new System.EventHandler(this.btnCosHiperbolic_Click);
			this.btnCosHiperbolic.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnTanHiperbolic
			// 
			this.btnTanHiperbolic.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnTanHiperbolic.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnTanHiperbolic.Location = new System.Drawing.Point(114, 119);
			this.btnTanHiperbolic.Name = "btnTanHiperbolic";
			this.btnTanHiperbolic.Size = new System.Drawing.Size(45, 30);
			this.btnTanHiperbolic.TabIndex = 49;
			this.btnTanHiperbolic.TabStop = false;
			this.btnTanHiperbolic.Text = "tanh";
			this.btnTanHiperbolic.UseCompatibleTextRendering = true;
			this.btnTanHiperbolic.UseVisualStyleBackColor = false;
			this.btnTanHiperbolic.Click += new System.EventHandler(this.btnTanHiperbolic_Click);
			this.btnTanHiperbolic.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnSinHiperbolicInv
			// 
			this.btnSinHiperbolicInv.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSinHiperbolicInv.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnSinHiperbolicInv.Location = new System.Drawing.Point(12, 155);
			this.btnSinHiperbolicInv.Name = "btnSinHiperbolicInv";
			this.btnSinHiperbolicInv.Size = new System.Drawing.Size(45, 30);
			this.btnSinHiperbolicInv.TabIndex = 54;
			this.btnSinHiperbolicInv.TabStop = false;
			this.btnSinHiperbolicInv.Text = "sinh⁻¹";
			this.btnSinHiperbolicInv.UseCompatibleTextRendering = true;
			this.btnSinHiperbolicInv.UseVisualStyleBackColor = false;
			this.btnSinHiperbolicInv.Click += new System.EventHandler(this.btnSinHiperbolicInv_Click);
			this.btnSinHiperbolicInv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnCosHiperbolicInv
			// 
			this.btnCosHiperbolicInv.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCosHiperbolicInv.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnCosHiperbolicInv.Location = new System.Drawing.Point(63, 155);
			this.btnCosHiperbolicInv.Name = "btnCosHiperbolicInv";
			this.btnCosHiperbolicInv.Size = new System.Drawing.Size(45, 30);
			this.btnCosHiperbolicInv.TabIndex = 53;
			this.btnCosHiperbolicInv.TabStop = false;
			this.btnCosHiperbolicInv.Text = "cosh⁻¹";
			this.btnCosHiperbolicInv.UseCompatibleTextRendering = true;
			this.btnCosHiperbolicInv.UseVisualStyleBackColor = false;
			this.btnCosHiperbolicInv.Click += new System.EventHandler(this.btnCosHiperbolicInv_Click);
			this.btnCosHiperbolicInv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// btnTanHiperbolicInv
			// 
			this.btnTanHiperbolicInv.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnTanHiperbolicInv.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnTanHiperbolicInv.Location = new System.Drawing.Point(114, 155);
			this.btnTanHiperbolicInv.Name = "btnTanHiperbolicInv";
			this.btnTanHiperbolicInv.Size = new System.Drawing.Size(45, 30);
			this.btnTanHiperbolicInv.TabIndex = 52;
			this.btnTanHiperbolicInv.TabStop = false;
			this.btnTanHiperbolicInv.Text = "tanh⁻¹";
			this.btnTanHiperbolicInv.UseCompatibleTextRendering = true;
			this.btnTanHiperbolicInv.UseVisualStyleBackColor = false;
			this.btnTanHiperbolicInv.Click += new System.EventHandler(this.btnTanHiperbolicInv_Click);
			this.btnTanHiperbolicInv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(459, 231);
			this.Controls.Add(this.btnSinHiperbolicInv);
			this.Controls.Add(this.btnCosHiperbolicInv);
			this.Controls.Add(this.btnTanHiperbolicInv);
			this.Controls.Add(this.btnSinHiperbolic);
			this.Controls.Add(this.btnCosHiperbolic);
			this.Controls.Add(this.btnTanHiperbolic);
			this.Controls.Add(this.btnPi);
			this.Controls.Add(this.btnCubeRoot);
			this.Controls.Add(this.btnPercent);
			this.Controls.Add(this.tbResult);
			this.Controls.Add(this.btnCalculate);
			this.Controls.Add(this.btnDecSeparator);
			this.Controls.Add(this.btnSwitchSign);
			this.Controls.Add(this.btnSquare);
			this.Controls.Add(this.btnCube);
			this.Controls.Add(this.btnSquareRoot);
			this.Controls.Add(this.btnInv);
			this.Controls.Add(this.btnExp);
			this.Controls.Add(this.btnE);
			this.Controls.Add(this.btnNthRoot);
			this.Controls.Add(this.btnPow);
			this.Controls.Add(this.btnFactorial);
			this.Controls.Add(this.btnMod);
			this.Controls.Add(this.btnSin);
			this.Controls.Add(this.btnCos);
			this.Controls.Add(this.btnTan);
			this.Controls.Add(this.btnLog);
			this.Controls.Add(this.btnLn);
			this.Controls.Add(this.btnSinInv);
			this.Controls.Add(this.btnCosInv);
			this.Controls.Add(this.btnTanInv);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.btnDivide);
			this.Controls.Add(this.btnMultiply);
			this.Controls.Add(this.btnSubtract);
			this.Controls.Add(this.btnCE);
			this.Controls.Add(this.btnC);
			this.Controls.Add(this.btnBackspace);
			this.Controls.Add(this.btn0);
			this.Controls.Add(this.btn9);
			this.Controls.Add(this.btn8);
			this.Controls.Add(this.btn7);
			this.Controls.Add(this.btn6);
			this.Controls.Add(this.btn5);
			this.Controls.Add(this.btn4);
			this.Controls.Add(this.btn3);
			this.Controls.Add(this.btn2);
			this.Controls.Add(this.btn1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Znanstveni kalkulator";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyUp);
			this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_MouseClick);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btn1;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn3;
		private System.Windows.Forms.Button btn4;
		private System.Windows.Forms.Button btn5;
		private System.Windows.Forms.Button btn6;
		private System.Windows.Forms.Button btn7;
		private System.Windows.Forms.Button btn8;
		private System.Windows.Forms.Button btn9;
		private System.Windows.Forms.Button btn0;
		private System.Windows.Forms.Button btnBackspace;
		private System.Windows.Forms.Button btnC;
		private System.Windows.Forms.Button btnCE;
		private System.Windows.Forms.Button btnSubtract;
		private System.Windows.Forms.Button btnMultiply;
		private System.Windows.Forms.Button btnDivide;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Button btnTanInv;
		private System.Windows.Forms.Button btnCosInv;
		private System.Windows.Forms.Button btnSinInv;
		private System.Windows.Forms.Button btnLn;
		private System.Windows.Forms.Button btnLog;
		private System.Windows.Forms.Button btnTan;
		private System.Windows.Forms.Button btnCos;
		private System.Windows.Forms.Button btnSin;
		private System.Windows.Forms.Button btnMod;
		private System.Windows.Forms.Button btnFactorial;
		private System.Windows.Forms.Button btnPow;
		private System.Windows.Forms.Button btnNthRoot;
		private System.Windows.Forms.Button btnE;
		private System.Windows.Forms.Button btnExp;
		private System.Windows.Forms.Button btnInv;
		private System.Windows.Forms.Button btnSquareRoot;
		private System.Windows.Forms.Button btnCube;
		private System.Windows.Forms.Button btnSquare;
		private System.Windows.Forms.Button btnSwitchSign;
		private System.Windows.Forms.Button btnDecSeparator;
		private System.Windows.Forms.Button btnCalculate;
		private System.Windows.Forms.TextBox tbResult;
		private System.Windows.Forms.Button btnPercent;
		private System.Windows.Forms.Button btnCubeRoot;
		private System.Windows.Forms.Button btnPi;
		private System.Windows.Forms.Button btnSinHiperbolic;
		private System.Windows.Forms.Button btnCosHiperbolic;
		private System.Windows.Forms.Button btnTanHiperbolic;
		private System.Windows.Forms.Button btnSinHiperbolicInv;
		private System.Windows.Forms.Button btnCosHiperbolicInv;
		private System.Windows.Forms.Button btnTanHiperbolicInv;
	}
}

