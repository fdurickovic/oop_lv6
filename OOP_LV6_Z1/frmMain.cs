﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace OOP_LV6_Z1 {
	public partial class frmMain : Form {
		private double operand1 = 0;
		private string operation = "";
		private bool operationDone = false;
		private double operand2 => Double.Parse(tbResult.Text);

		private string decimalSeparator = CultureInfo
											.CurrentCulture
											.NumberFormat
											.NumberDecimalSeparator;

		public frmMain() => InitializeComponent();

		private void Form1_Load(object sender, EventArgs e)
			=> btnDecSeparator.Text = decimalSeparator;

		private void tbResult_TextChanged(object sender, EventArgs e) {
			if (tbResult.Text == "") {
				tbResult.Text = "0";
			}
			if (tbResult.Text.Length > 1 && tbResult.Text[0] == '0') {
				tbResult.Text = tbResult.Text.TrimStart('0');
			}
		}

		private void frmMain_KeyUp(object sender, KeyEventArgs e) {

			switch (e.KeyData) {
				case Keys.Back:
					btnBackspace.PerformClick();
					break;
				case Keys.Delete:
					btnCE.PerformClick();
					break;
				case Keys.Escape:
					btnC.PerformClick();
					break;
				case Keys.Control | Keys.Alt | Keys.D3:
					btnPow.PerformClick();
					break;
				case Keys.Shift | Keys.D1:
					btnFactorial.PerformClick();
					break;
				case Keys.Shift | Keys.D5:
					btnPercent.PerformClick();
					break;

				case Keys.Shift | Keys.D7:
				case Keys.Divide:
					btnDivide.PerformClick();
					break;

				case Keys.Multiply:
				case Keys.Oemplus | Keys.Shift:
					btnMultiply.PerformClick();
					break;

				case Keys.Oemplus:
				case Keys.Add:
					btnAdd.PerformClick();
					break;

				case Keys.Subtract:
				case Keys.OemMinus:
					btnSubtract.PerformClick();
					break;

				case Keys.Shift | Keys.D0:
				case Keys.Return:
					btnCalculate.PerformClick();
					break;

				case Keys.OemPeriod:
				case Keys.Oemcomma:
				case Keys.Decimal:
					btnDecSeparator.PerformClick();
					break;

				case Keys.D0:
				case Keys.NumPad0:
					btn0.PerformClick();
					break;
				case Keys.D1:
				case Keys.NumPad1:
					btn1.PerformClick();
					break;
				case Keys.D2:
				case Keys.NumPad2:
					btn2.PerformClick();
					break;
				case Keys.D3:
				case Keys.NumPad3:
					btn3.PerformClick();
					break;
				case Keys.D4:
				case Keys.NumPad4:
					btn4.PerformClick();
					break;
				case Keys.D5:
				case Keys.NumPad5:
					btn5.PerformClick();
					break;
				case Keys.D6:
				case Keys.NumPad6:
					btn6.PerformClick();
					break;
				case Keys.D7:
				case Keys.NumPad7:
					btn7.PerformClick();
					break;
				case Keys.D8:
				case Keys.NumPad8:
					btn8.PerformClick();
					break;
				case Keys.D9:
				case Keys.NumPad9:
					btn9.PerformClick();
					break;
			}
		}

		private void btnOperator_Click(object sender, EventArgs e) {
			Button button = (Button)sender;
			if (operand1 != 0) {
				btnCalculate.PerformClick();
				SetOperation(button);
				operationDone = true;
			} else {
				SetOperation(button);
				operand1 = operand2;
			}
			operationDone = true;
		}

		private void btnNumeric_Click(object sender, EventArgs e) {
			Button senderBtn = sender as Button;
			if ((tbResult.Text == "0") || operationDone) {
				tbResult.Clear();
			}

			operationDone = false;
			if (tbResult.Text == "0") {
				tbResult.Text = "";
			}

			if (tbResult.Text.Contains(decimalSeparator)
				&& senderBtn.Name == "btnDecSeparator") {
				return;
			}

			tbResult.Text += senderBtn.Text;
		}

		private void btnSwitchSign_Click(object sender, EventArgs e)
			=> tbResult.Text = (operand2 * -1).ToString();

		private void btnSin_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Sin(operand2).ToString();

		private void btnCos_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Cos(operand2).ToString();

		private void btnTan_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Tan(operand2).ToString();

		private void btnSinInv_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Asin(operand2).ToString();

		private void btnCosInv_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Acos(operand2).ToString();

		private void btnTanInv_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Atan(operand2).ToString();

		private void btnSinHiperbolic_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Sinh(operand2).ToString();

		private void btnCosHiperbolic_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Cosh(operand2).ToString();

		private void btnTanHiperbolic_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Tanh(operand2).ToString();

		private void btnSinHiperbolicInv_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Log(operand2 + Math.Sqrt(Math.Pow(operand2, 2) + 1)).ToString();

		private void btnCosHiperbolicInv_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Log(operand2 + Math.Sqrt(Math.Pow(operand2, 2) - 1)).ToString();

		private void btnTanHiperbolicInv_Click(object sender, EventArgs e)
			=> tbResult.Text = (0.5 * Math.Log((1 + operand2) / (1 - operand2))).ToString();

		private void btnCubeRoot_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Pow(operand2, 1 / 3).ToString();

		private void btnLog_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Log10(operand2).ToString();

		private void btnLn_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Log(operand2).ToString();

		private void btnE_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Pow(10, operand2).ToString();

		private void btnSquareRoot_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Sqrt(operand2).ToString();

		private void btnFactorial_Click(object sender, EventArgs e)
			=> tbResult.Text = Factorial(operand2).ToString();

		private void btnSquare_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Pow(operand2, 2).ToString();

		private void btnCube_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Pow(operand2, 3).ToString();

		private void btnInv_Click(object sender, EventArgs e)
			=> tbResult.Text = (1 / operand2).ToString();

		private void btnExp_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.Exp(operand2).ToString();

		private void btnPi_Click(object sender, EventArgs e)
			=> tbResult.Text = Math.PI.ToString();

		private void btnPercent_Click(object sender, EventArgs e) {
			tbResult.Text = (operand1 * (operand2 / 100)).ToString();
			btnCalculate.PerformClick();
		}

		private void btnCalculate_Click(object sender, EventArgs e) {
			switch (operation) {
				case "%%":
					tbResult.Text = (operand1 % operand2).ToString();
					break;
				case "+":
					tbResult.Text = (operand1 + operand2).ToString();
					break;
				case "-":
					tbResult.Text = (operand1 - operand2).ToString();
					break;
				case "/":
					tbResult.Text = (operand1 / operand2).ToString();
					break;
				case "*":
					tbResult.Text = (operand1 * operand2).ToString();
					break;
				case "^":
					tbResult.Text = Math.Pow(operand1, operand2).ToString();
					break;
				case "√":
					tbResult.Text = Math.Pow(operand1, 1 / operand2).ToString();
					break;
			}
			operand1 = 0;
		}

		private void btnBackspace_Click(object sender, EventArgs e)
			=> tbResult.Text = tbResult.Text.Remove(tbResult.Text.Length - 1);

		private void btnCE_Click(object sender, EventArgs e)
			=> tbResult.Text = "";

		private void btnC_Click(object sender, EventArgs e) {
			tbResult.Text = "";
			operation = "";
			operationDone = false;
		}

		private void Button_MouseClick(object sender, MouseEventArgs e)
			=> tbResult.Focus();

		private static double Factorial(double n) {
			if (n <= 1) {
				return 1;
			}
			double result = n;
			for (int i = 1; i < n; i++) {
				result = result * i;
			}
			return result;
		}

		private void SetOperation(Button button) {
			switch (button.Text) {
				case "Mod":
					operation = "%%";
					break;
				case "+":
					operation = "+";
					break;
				case "-":
					operation = "-";
					break;
				case "÷":
					operation = "/";
					break;
				case "×":
					operation = "*";
					break;
				case "xⁿ":
					operation = "^";
					break;
				case "ⁿ√x":
					operation = "√";
					break;
			}
		}
	}
}
