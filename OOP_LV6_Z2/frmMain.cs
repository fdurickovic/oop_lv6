﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace OOP_LV6_Z2 {
	public partial class frmMain : Form {
		private const int MAX_ATTEMPTS = 10;
		private string[] allWords;
		private string currentWord;
		private string attemptedLetters = "";
		private int attempts = MAX_ATTEMPTS;
		private Random rng = new Random();
		public frmMain() => InitializeComponent();

		private void tbGuess_KeyUp(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Enter) {
				CheckGuess();
			}
		}
		private void ResizeForm() {
			int lblWidth = lbWord.Width;
			int panelWidth = panel1.Width;
			int formWidth = Math.Max(lblWidth, panelWidth) + 24;
			Width = formWidth;
			panel1.Left = (formWidth / 2) - (panelWidth / 2) - 12;
			lbWord.Left = (formWidth / 2) - (lblWidth / 2) - 12;
		}
		private void Form1_Load(object sender, EventArgs e) {
			string filename = "words.txt";
			do {
				if (ofdWordsFile.ShowDialog() == DialogResult.OK) {
					filename = ofdWordsFile.FileName;
				}
				if (!File.Exists(filename) || File.ReadAllLines(filename).Length < 1) {
					MessageBox.Show("Datoteka ne postoji ili nema pojmova u njoj!");
				}
			} while (!File.Exists(filename) || File.ReadAllLines(filename).Length < 1);

			allWords = File.ReadAllLines(filename);
			SetNewWord();
		}

		private void SetNewWord() {
			int rndIndex = rng.Next(0, allWords.Length);
			currentWord = allWords[rndIndex].ToUpper();
			attempts = MAX_ATTEMPTS;
			attemptedLetters = "";
			lbRemainingAttempts.Text = attempts.ToString();
			lbWord.Text = Regex.Replace(String.Join(" ", currentWord.ToCharArray()), "[^ ]", "_");
			ResizeForm();
		}
		private void CheckGuess() {
			if (tbGuess.Text.Trim() == "") {
				return;
			}
			if (attemptedLetters.Contains(tbGuess.Text)) {
				MessageBox.Show("Ovo slovo je već isprobano.");
				return;
			}

			attemptedLetters += tbGuess.Text;
			if (!currentWord.Contains(tbGuess.Text)) {
				attempts--;
				lbRemainingAttempts.Text = attempts.ToString();
			}
			string regexPattern = "[^ " + attemptedLetters + "]";
			lbWord.Text = Regex.Replace(String.Join(" ", currentWord.ToCharArray()), regexPattern, "_");
			tbGuess.Text = "";
			ResizeForm();
			if (Regex.Replace(currentWord, $"[ {attemptedLetters}]", "") == "") {
				MessageBox.Show("Pobjedili ste. ヽ(•‿•)ノ");
				SetNewWord();
			}

			if (attempts == 0) {
				MessageBox.Show($"Izgubili ste. (✖╭╮✖)\nTražena riječ je bila {currentWord}");
				SetNewWord();
			}
		}
	}
}
