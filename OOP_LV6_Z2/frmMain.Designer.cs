﻿namespace OOP_LV6_Z2 {
	partial class frmMain {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
			this.tbGuess = new System.Windows.Forms.TextBox();
			this.ofdWordsFile = new System.Windows.Forms.OpenFileDialog();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.lbRemainingAttempts = new System.Windows.Forms.Label();
			this.lbWord = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tbGuess
			// 
			this.tbGuess.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.tbGuess.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tbGuess.Location = new System.Drawing.Point(52, 5);
			this.tbGuess.MaxLength = 1;
			this.tbGuess.Name = "tbGuess";
			this.tbGuess.Size = new System.Drawing.Size(22, 22);
			this.tbGuess.TabIndex = 0;
			this.tbGuess.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.tbGuess.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbGuess_KeyUp);
			// 
			// ofdWordsFile
			// 
			this.ofdWordsFile.DefaultExt = "txt";
			this.ofdWordsFile.FileName = "words.txt";
			this.ofdWordsFile.Title = "Otvori datoteku s pojmovima";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label1.Location = new System.Drawing.Point(104, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(147, 15);
			this.label1.TabIndex = 1;
			this.label1.Text = "Broj preostalih pokušaja: ";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label2.Location = new System.Drawing.Point(3, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(43, 15);
			this.label2.TabIndex = 2;
			this.label2.Text = "Slovo: ";
			// 
			// lbRemainingAttempts
			// 
			this.lbRemainingAttempts.AutoSize = true;
			this.lbRemainingAttempts.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.lbRemainingAttempts.Location = new System.Drawing.Point(257, 9);
			this.lbRemainingAttempts.Name = "lbRemainingAttempts";
			this.lbRemainingAttempts.Size = new System.Drawing.Size(0, 15);
			this.lbRemainingAttempts.TabIndex = 3;
			// 
			// lbWord
			// 
			this.lbWord.AutoSize = true;
			this.lbWord.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.lbWord.Location = new System.Drawing.Point(12, 9);
			this.lbWord.Name = "lbWord";
			this.lbWord.Size = new System.Drawing.Size(0, 72);
			this.lbWord.TabIndex = 4;
			this.lbWord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel1
			// 
			this.panel1.AutoSize = true;
			this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.tbGuess);
			this.panel1.Controls.Add(this.lbRemainingAttempts);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(255, 212);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(260, 30);
			this.panel1.TabIndex = 5;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(771, 254);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.lbWord);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Vješala";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox tbGuess;
		private System.Windows.Forms.OpenFileDialog ofdWordsFile;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lbRemainingAttempts;
		private System.Windows.Forms.Label lbWord;
		private System.Windows.Forms.Panel panel1;
	}
}

